# node-red-contrib-message-gate

##### <a name="Francais"></a>Français
[English version](#English)

Composant permettant de gérer une Porte que l'on ouvre ou ferme dynamiquement pour laisser passer les messages.

Node-red fonctionne sous la forme d'événement en entrer et en sortie. Un événement génère un message qui est transmis de nœud en nœud, jusqu'au dernier.

Le problème et que rien en natif et simple ne permet d'enregistrer un état. Ce qui peut être une limitation pour mettre en place des traitements conditionnels.

Pour gérer cela nous avons 2 composants :
* jvmg_gate
* jvmg_key

## jvmg_gate
C'est le nœud de base, c'est lui qui vas laisser passer ou pas le message. On peux ainsi l'ajouter dans notre flux devant un traitement pour le bloquer si nécessaire.

Son paramétrage est simple, il faut lui indiquer la clé de référence qui gère l'ouverture ou la fermeture. Ainsi que l'état par défaut si la clé est dans un état indéfinie (par encore initialisé).

## jvmg_key
C'est le composant que l'on utilise pour alimenter la clé. En guise de paramétrage on indique simplement la clé correspondante.

Pour changer d'état le nœud va lire msg.payload sous la forme d'un boolean.

## Utilisation
Il est possible d'utiliser plusieurs Gate pour une seule Key et il est possible d'utiliser plusieurs Key pour une seule Gate à partir du moment où elle fond référence à la même clé.

## Exemple d'utilisation
Voici un exemple simple.

![Exemple Hello World](exemple01.png)

```json
[
   {
      "id":"7fdec59c.0c8bd4",
      "type":"debug",
      "z":"d1f77b9c.d789f8",
      "name":"",
      "active":true,
      "tosidebar":true,
      "console":false,
      "tostatus":false,
      "complete":"payload",
      "x":490,
      "y":200,
      "wires":[

      ]
   },
   {
      "id":"5a79e052.9523e",
      "type":"inject",
      "z":"d1f77b9c.d789f8",
      "name":"",
      "topic":"",
      "payload":"Hello World!",
      "payloadType":"str",
      "repeat":"",
      "crontab":"",
      "once":false,
      "onceDelay":0.1,
      "x":170,
      "y":200,
      "wires":[
         [
            "d86fc0.eec6084"
         ]
      ]
   },
   {
      "id":"cfde8d22.9f5b38",
      "type":"inject",
      "z":"d1f77b9c.d789f8",
      "name":"",
      "topic":"",
      "payload":"true",
      "payloadType":"bool",
      "repeat":"",
      "crontab":"",
      "once":false,
      "onceDelay":0.1,
      "x":170,
      "y":60,
      "wires":[
         [
            "56d84127.5bf7f8"
         ]
      ]
   },
   {
      "id":"7724dff0.40983",
      "type":"inject",
      "z":"d1f77b9c.d789f8",
      "name":"",
      "topic":"",
      "payload":"false",
      "payloadType":"bool",
      "repeat":"",
      "crontab":"",
      "once":false,
      "onceDelay":0.1,
      "x":170,
      "y":100,
      "wires":[
         [
            "56d84127.5bf7f8"
         ]
      ]
   },
   {
      "id":"56d84127.5bf7f8",
      "type":"jvmg_key",
      "z":"d1f77b9c.d789f8",
      "name":"",
      "trigger":"68b506ff.e13fc8",
      "x":370,
      "y":80,
      "wires":[

      ]
   },
   {
      "id":"d86fc0.eec6084",
      "type":"jvmg_gate",
      "z":"d1f77b9c.d789f8",
      "name":"",
      "default":true,
      "trigger":"68b506ff.e13fc8",
      "x":330,
      "y":200,
      "wires":[
         [
            "7fdec59c.0c8bd4"
         ]
      ]
   },
   {
      "id":"68b506ff.e13fc8",
      "type":"jvmg_trigger",
      "z":"",
      "trigger":"MyHelloWorldKey"
   }
]
```

Dans cet exemple, au démarrage la clé n'est pas définie, la porte prend donc son état par défaut, et donc peut laisser passer le message. C’est-à-dire ici que si je clique sur "Hello World" je vais avoir en sortie de debug "Hello World". Cela sera aussi le cas si je clique sur true ce qui ouvre la porte. Dans le cas où je clique sur False, la porte seras fermer et je pourrais cliquer au temps que je veux sur "Hello World" rien ne se passera.

----------
##### <a name="English"></a>English

[Version Française](#Francais)


Component used to manage a gate that is opened or closed dynamically to let messages through.

Node-red works white input and output event. An event generates a message that is passed from node to node, to the last node.

The problem is that nothing in native and simple allows to record a state. This can be a limitation in setting up conditional treatments.

To manage this we have 2 components :
* jvmg_gate
* jvmg_key

## jvmg_gate
It is the basic node, it is he who will let the message pass or not. We can add it in our flow in front of a treatment to block it if necessary.

Its parameter setting is simple, it must be indicated the reference key which manages the opening or the closing. As well as the default state if the key is in an undefined state (not yet initialized).

## jvmg_key
This is the component used to power the key. The corresponding key is simply entered as a parameter.

To change state the node will read msg.payload as a boolean.

## Utilisation
It is possible to use several Gates for a single Key and it is possible to use several Key for a single Gate from the moment it refers to the same key.

## Exemple d'utilisation
A simple example.

![Exemple Hello World](exemple01.png)

```json
[
   {
      "id":"7fdec59c.0c8bd4",
      "type":"debug",
      "z":"d1f77b9c.d789f8",
      "name":"",
      "active":true,
      "tosidebar":true,
      "console":false,
      "tostatus":false,
      "complete":"payload",
      "x":490,
      "y":200,
      "wires":[

      ]
   },
   {
      "id":"5a79e052.9523e",
      "type":"inject",
      "z":"d1f77b9c.d789f8",
      "name":"",
      "topic":"",
      "payload":"Hello World!",
      "payloadType":"str",
      "repeat":"",
      "crontab":"",
      "once":false,
      "onceDelay":0.1,
      "x":170,
      "y":200,
      "wires":[
         [
            "d86fc0.eec6084"
         ]
      ]
   },
   {
      "id":"cfde8d22.9f5b38",
      "type":"inject",
      "z":"d1f77b9c.d789f8",
      "name":"",
      "topic":"",
      "payload":"true",
      "payloadType":"bool",
      "repeat":"",
      "crontab":"",
      "once":false,
      "onceDelay":0.1,
      "x":170,
      "y":60,
      "wires":[
         [
            "56d84127.5bf7f8"
         ]
      ]
   },
   {
      "id":"7724dff0.40983",
      "type":"inject",
      "z":"d1f77b9c.d789f8",
      "name":"",
      "topic":"",
      "payload":"false",
      "payloadType":"bool",
      "repeat":"",
      "crontab":"",
      "once":false,
      "onceDelay":0.1,
      "x":170,
      "y":100,
      "wires":[
         [
            "56d84127.5bf7f8"
         ]
      ]
   },
   {
      "id":"56d84127.5bf7f8",
      "type":"jvmg_key",
      "z":"d1f77b9c.d789f8",
      "name":"",
      "trigger":"68b506ff.e13fc8",
      "x":370,
      "y":80,
      "wires":[

      ]
   },
   {
      "id":"d86fc0.eec6084",
      "type":"jvmg_gate",
      "z":"d1f77b9c.d789f8",
      "name":"",
      "default":true,
      "trigger":"68b506ff.e13fc8",
      "x":330,
      "y":200,
      "wires":[
         [
            "7fdec59c.0c8bd4"
         ]
      ]
   },
   {
      "id":"68b506ff.e13fc8",
      "type":"jvmg_trigger",
      "z":"",
      "trigger":"MyHelloWorldKey"
   }
]
```

In this example, at startup the key is not defined, so the door takes its default state, and can let the message through. That is to say here that if I click on "Hello World" I will have "Hello World" debugged. This will also be the case if I click true which opens the door. In case I click on False, the door will be closed and I could click at the time I want on "Hello World" nothing will happen.

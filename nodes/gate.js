/**
 * Copyright Julien Vinber, julien@vinber.fr
 *
 * Licensed under the CeCILL-C and LGPL V3.
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html
 * http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
 *
 * https://www.gnu.org/licenses/lgpl-3.0-standalone.html
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 **/

module.exports = function (RED) {
    function Gate(config) {
        RED.nodes.createNode(this, config, '');
        let node = this;

        node.eventManager = require('./eventManager').Manager;

        node.defaultStatus = config.default;

        node.trigger = RED.nodes.getNode(config.trigger);

        node.globalContext = this.context().global;

        node.majStatus = function () {
            if (node.actif) {
                node.status({fill: "green", shape: "dot", text: RED._("gate.status.open")});
            } else {
                node.status({fill: "red", shape: "ring", text: RED._("gate.status.close")});
            }
        };

        node.majActif = function () {
            let value = node.trigger.getStat();

            if (value === undefined) {
                value = node.defaultStatus;
            }
            node.actif = value;
            node.majStatus();
        };
        node.majActif();

        node.on('input', function (msg) {
            node.majActif();

            if (node.actif) {
                node.send(msg);
            }
        });

        node.eventManager.on('change', function (){
            node.majActif();
        });
    }

    RED.nodes.registerType("jvmg_gate", Gate);
};
